module EvalSpec where


import Test.Hspec
import Parser
import Expr
import MiniRacketParser

import Eval
import Error

type ParseResult = Either ErrorType (Expr, String)

spec :: Spec
spec = do
    describe "eval expressions" $ do
        it "evaluates number: 1235" $ 
            evalString "1235" `shouldBe` Right (IntValue 1235)
        it "evaluates negative numbers: -12235" $
            evalString "-12235" `shouldBe` Right (IntValue (-12235))
        it "evaluates true" $
            evalString "true" `shouldBe` Right (BoolValue True)
        it "evaluates false" $
            evalString "false" `shouldBe` Right (BoolValue False)
        
    describe "eval maths" $ do
        it "evaluates addition: (+ 1 2)" $
            evalString "(+ 1 2)" `shouldBe` Right (IntValue 3)
        it "evaluates subtraction: (- 5 3)" $
            evalString "(- 5 3)" `shouldBe` Right (IntValue 2)
        it "evaluates multiplication: (* 2 3)" $
            evalString "(* 2 3)" `shouldBe` Right (IntValue 6)
        it "evaluates division: (div 6 2)" $
            evalString "(div 6 2)" `shouldBe` Right (IntValue 3)
        it "evaluates modulo: (mod 7 4)" $
            evalString "(mod 7 4)" `shouldBe` Right (IntValue 3)

    describe "eval comparisons" $ do
        it "evaluates equal?: (equal? 1 1)" $
            evalString "(equal? 1 1)" `shouldBe` Right (BoolValue True)
        it "evaluates <: (< 2 3)" $
            evalString "(< 2 3)" `shouldBe` Right (BoolValue True)
        it "evaluates <=: (<= 3 3)" $
            evalString "(<= 3 3)" `shouldBe` Right (BoolValue True)
        it "evaluates >: (> 5 4)" $
            evalString "(> 5 4)" `shouldBe` Right (BoolValue True)
        it "evaluates >=: (>= 6 6)" $
            evalString "(>= 6 6)" `shouldBe` Right (BoolValue True)

    describe "eval boolean operators" $ do
        it "evaluates and: (and true false)" $
            evalString "(and true false)" `shouldBe` Right (BoolValue False)
        it "evaluates or: (or false true)" $
            evalString "(or false true)" `shouldBe` Right (BoolValue True)
        it "evaluates not: (not true)" $
            evalString "(not true)" `shouldBe` Right (BoolValue False)

    describe "evaluates expressions" $ do
        it "evaluatates var expression" $
            evalString "x" `shouldBe` Left (NoSymbol "symbol x not found")
        it "evaluates var expression with existing binding" $
            evalString "(let (x 42) x)" `shouldBe` Right (IntValue 42)
        it "evaluates if expression: (if true 1 2)" $
            evalString "(if true 1 2)" `shouldBe` Right (IntValue 1)
        it "evaluates if/let expression:" $
            evalString "(let (x (if (and (< 2 5) (equal? 6 30)) 2 6)) (* x 30) )" `shouldBe` Right (IntValue 180)
        it "evaluates if expression with boolean result" $
            evalString "(if (equal? 1 1) true false)" `shouldBe` Right (BoolValue True)
        it "evaluates let/lambda/ recursrive expression:" $
            evalString "(let (f (lambda (x) (if (equal? x 0) 0 (+ x (f (- x 1)))))) (f 10))" `shouldBe` Right (IntValue 55) 
        it "evaluates complex let/lambda/recursive expression:" $
            evalString "(let (f (lambda (x) (if (equal? x 0) 0 (+ x (f (- x 1)))))) (let (g (lambda (y) (f y))) (g 5)))" `shouldBe` Right (IntValue 15)





