module MiniRacketParserSpec where 

import Test.Hspec
import Parser
import Expr 
import MiniRacketParser
import Error

type ParseResult = Either ErrorType (Expr, String)

expr :: Either ErrorType (a2, b) -> a2
expr (Right (e, _)) = e 
expr (Left (SyntaxError msg)) = error msg
expr (Left (ParseError msg)) = error msg
expr (Left NoParse) = error "no matching parse"
expr _ = error "expr in MiniRacketParser.hs is not fully implemented yet..."

spec :: Spec 
spec = do 
    describe "parse literals" $ do
        it "parses number: 1235" $ 
            parseString "1235" `shouldBe` Right (LiteralExpr (IntValue 1235),"")
        it "parses negative numbers: -12235" $
            parseString "-12235" `shouldBe` Right (LiteralExpr (IntValue (-12235)), "")
        it "parses true" $
            parseString "true" `shouldBe` Right (LiteralExpr (BoolValue True), "")
        it "parses false" $
            parseString "false" `shouldBe` Right (LiteralExpr (BoolValue False), "")

    describe "parse maths" $ do
        it "parses addition: (+ 1 2)" $
            parseString "(+ 1 2)" `shouldBe` Right (MathExpr Add [LiteralExpr (IntValue 1), LiteralExpr (IntValue 2)], "")
        it "parses subtraction: (- 5 3)" $
            parseString "(- 5 3)" `shouldBe` Right (MathExpr Sub [LiteralExpr (IntValue 5), LiteralExpr (IntValue 3)], "")
        it "parses multiplication: (* 2 3)" $
            parseString "(* 2 3)" `shouldBe` Right (MathExpr Mul [LiteralExpr (IntValue 2), LiteralExpr (IntValue 3)], "")
        it "parses division: (div 6 2)" $
            parseString "(div 6 2)" `shouldBe` Right (MathExpr Div [LiteralExpr (IntValue 6), LiteralExpr (IntValue 2)], "")
        it "parses modulo: (mod 7 4)" $
            parseString "(mod 7 4)" `shouldBe` Right (MathExpr Mod [LiteralExpr (IntValue 7), LiteralExpr (IntValue 4)], "")

    describe "parse comparisons" $ do
        it "parses equal?: (equal? 1 1)" $
            parseString "(equal? 1 1)" `shouldBe` Right (CompExpr Eq (LiteralExpr (IntValue 1)) (LiteralExpr (IntValue 1)), "")
        it "parses <: (< 2 3)" $
            parseString "(< 2 3)" `shouldBe` Right (CompExpr Lt (LiteralExpr (IntValue 2)) (LiteralExpr (IntValue 3)), "")
        it "parses <=: (<= 3 3)" $
            parseString "(<= 3 3)" `shouldBe` Right (CompExpr LEq (LiteralExpr (IntValue 3)) (LiteralExpr (IntValue 3)), "")
        it "parses >: (> 5 4)" $
            parseString "(> 5 4)" `shouldBe` Right (CompExpr Gt (LiteralExpr (IntValue 5)) (LiteralExpr (IntValue 4)), "")
        it "parses >=: (>= 6 6)" $
            parseString "(>= 6 6)" `shouldBe` Right (CompExpr GEq (LiteralExpr (IntValue 6)) (LiteralExpr (IntValue 6)), "")

    describe "parse boolean operators" $ do
        it "parses and: (and true false)" $
            parseString "(and true false)" `shouldBe` Right (BoolExpr And [LiteralExpr (BoolValue True), LiteralExpr (BoolValue False)], "")
        it "parses or: (or false true)" $
            parseString "(or false true)" `shouldBe` Right (BoolExpr Or [LiteralExpr (BoolValue False), LiteralExpr (BoolValue True)], "")
        it "parses not: (not true)" $
            parseString "(not true)" `shouldBe` Right (NotExpr (LiteralExpr (BoolValue True)), "")

    describe "parse negate atom" $ do
        it "parses negation of a number: (- 42)" $
            parseString "(- 42)" `shouldBe` Right (NegateExpr (LiteralExpr (IntValue 42)), "")
        it "parses negation of a number: (- 1)" $
            parseString "(- 1)" `shouldBe` Right (NegateExpr (LiteralExpr (IntValue 1)),"")

    describe "parse var expression" $ do
        it "parses a variable: x" $
            parseString "x" `shouldBe` Right (VarExpr "x", "")
        it "parses a variable: z" $
            parseString "z" `shouldBe` Right (VarExpr "z", "")

    describe "parse if expression" $ do
        it "parses if expression: (if true 1 2)" $
            parseString "(if true 1 2)" `shouldBe` Right (IfExpr (LiteralExpr (BoolValue True)) (LiteralExpr (IntValue 1)) (LiteralExpr (IntValue 2)), "")
        it "parses if expression: (if (< 4 5) true false)" $
            parseString "(if (< 4 5) true false)" `shouldBe` Right (IfExpr (CompExpr Lt (LiteralExpr (IntValue 4)) (LiteralExpr (IntValue 5))) (LiteralExpr (BoolValue True)) (LiteralExpr (BoolValue False)),"")

    describe "parse let expression" $ do
        it "parses let expression: (let (x 1) (+ x 2))" $
            parseString "(let (x 1) (+ x 2))" `shouldBe` Right (LetExpr "x" (LiteralExpr (IntValue 1)) (MathExpr Add [VarExpr "x", LiteralExpr (IntValue 2)]), "")
        it "parses let expression" $
            parseString "(let (x (if (and (< 2 5) (equal? 6 30)) 2 6)) (* x 30) )" `shouldBe` Right (LetExpr "x" (IfExpr (BoolExpr And [CompExpr Lt (LiteralExpr (IntValue 2)) (LiteralExpr (IntValue 5)),CompExpr Eq (LiteralExpr (IntValue 6)) (LiteralExpr (IntValue 30))]) (LiteralExpr (IntValue 2)) (LiteralExpr (IntValue 6))) (MathExpr Mul [VarExpr "x",LiteralExpr (IntValue 30)]),"")

    describe "parse lambda expression" $ do
        it "parses lambda expression: (lambda (x) (+ x 1))" $
            parseString "(let (f (lambda (x) (if (equal? x 0) 0 (* x (f (- x 1)))))) (f 10))" `shouldBe` Right (LetExpr "f" (LambdaExpr "x" (IfExpr (CompExpr Eq (VarExpr "x") (LiteralExpr (IntValue 0))) (LiteralExpr (IntValue 0)) (MathExpr Mul [VarExpr "x",ApplyExpr (VarExpr "f") (MathExpr Sub [VarExpr "x",LiteralExpr (IntValue 1)])]))) (ApplyExpr (VarExpr "f") (LiteralExpr (IntValue 10))),"")

    describe "parse callfun expression" $ do
        it "parses callfun expression with a simple lambda: (callfun (lambda (x) (+ x 1)) 5)" $
            parseString "(callfun (lambda (x) (+ x 1)) 5)" `shouldBe`
            Right (ApplyExpr (ApplyExpr (VarExpr "callfun") (LambdaExpr "x" (MathExpr Add [VarExpr "x",LiteralExpr (IntValue 1)]))) (LiteralExpr (IntValue 5)),"")

    describe "parse recursive function calls" $ do
        it "parses recursive function call: (factorial 5)" $
            parseString "(factorial 5)" `shouldBe`
            Right (ApplyExpr (VarExpr "factorial") (LiteralExpr (IntValue 5)), "")

 
