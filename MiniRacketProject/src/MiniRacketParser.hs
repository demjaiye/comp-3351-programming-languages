module MiniRacketParser where

import Parser
import Expr
import Control.Applicative
import Error ( ErrorType ) 

parseBool :: Parser Bool
parseBool = do
        parseKeyword "true"
        return True
        <|> do
            parseKeyword "false"
            return False


-- parse binary bool operations
-- TODO: implement parsing bool operations which have 
--   two parameters, these are 'and' and 'or'
parseBoolOp :: Parser BoolOp
-- parseBoolOp = failParse "not implemented"
parseBoolOp = do
    symbol "and" >> return And
    <|> do symbol "or" >> return Or
    

-- parse math operations and return the MathOp
-- TODO: Add the other math operations: *, div, mod
parseMathOp :: Parser MathOp
parseMathOp =
    do symbol "+" >> return Add
    <|> do symbol "-" >> return Sub
    <|> do symbol "*" >> return Mul
    <|> do symbol "div" >> return Div
    <|> do symbol "mod" >> return Mod
    
    

-- parse the comparison operations and return the corresponding  CompOp
-- TODO: add the comparison operators: equal?, < 
parseCompOp :: Parser CompOp
-- parseCompOp = failParse "not implemented"
parseCompOp = do symbol "equal?" >> return Eq
    <|> do symbol "<=" >> return LEq
    <|> do symbol "<" >> return Lt
    <|> do symbol ">=" >> return GEq
    <|> do symbol ">" >> return Gt


-- a literal in MiniRacket is true, false, or a number
-- TODO: parse the literals: true, false, and numbers
literal :: Parser Value
-- literal = failParse "not implemented"
literal = do IntValue <$> natural
        <|> BoolValue <$> parseBool


-- parse a literal expression, which is just a literal
literalExpr :: Parser Expr
literalExpr = do
    LiteralExpr <$> literal


keywordList :: [String]
keywordList = ["false", "true", "not", "and", "or", "div", "mod", "equal?", "if", "let", "lambda"]

-- try to parse a keyword, otherwise it is a variable, this can be
-- used to check if the identifier we see (i.e., variable name) is
-- actually a keyword, which isn't legal
parseKeyword :: String -> Parser String
parseKeyword keyword = do
    -- all keywords follow the identifier rules, so we'll use that
    name <- identifier
    if name `elem` keywordList && keyword == name
    then return name
    else failParse $ "saw " ++ name ++ ", expected " ++ keyword


-- TODO: parse not expressions, note that "not" is a keyword,
-- (HINT: you should use parseKeyword)
notExpr :: Parser Expr
-- notExpr = failParse "not implemented"
notExpr = do
    parseKeyword "not"
    NotExpr <$> parseExpr

-- TODO: parse boolean expressions
-- a bool expression is the operator followed by one or more expressions
boolExpr :: Parser Expr
-- boolExpr = failParse "not implemented"
boolExpr = do
    op <- parseBoolOp
    exprs <- some parseExpr
    return  (BoolExpr op exprs)

-- TODO: parse maths expressions
-- a math expression is the operator followed by one or more expressions
mathExpr :: Parser Expr
-- mathExpr = failParse "not implemented"
mathExpr = do
    op <- parseMathOp
    exprs <- some parseExpr
    return (MathExpr op exprs)

-- a comparison expression is the comparison operator
--   followed by two expressions
compExpr :: Parser Expr
compExpr = CompExpr <$> parseCompOp <*> parseExpr <*> parseExpr

pairExpr :: Parser Expr
pairExpr = do
    expr1 <- parseExpr
    symbol "."
    PairExpr expr1 <$> parseExpr

-- note that this is syntactic sugar, cons is just replaced by a 
--    PairExpr abstract syntax tree 
consExpr :: Parser Expr 
consExpr = do 
    symbol "cons"
    expr1 <- parseExpr 
    PairExpr expr1 <$> parseExpr 

parseParens :: Parser Expr -> Parser Expr
parseParens p = do
    symbol "("
    e <- p
    symbol ")"
    return e

-- TODO: add the additional kinds of things that can be an atom:
--   an atom is either a var, a literal, or a negated atom
parseAtom :: Parser Expr
parseAtom = do
    literalExpr
    <|> negateAtom
    <|> varExpr



-- the main parsing function which alternates between all
-- the options you have for possible expressions
-- TODO: Add new expression types here
parseExpr :: Parser Expr
parseExpr = do
    parseAtom
    <|> parseParens notExpr
    <|> parseParens parseExpr
    <|> parseParens compExpr
    <|> parseParens pairExpr
    <|> parseParens consExpr
    <|> parseParens boolExpr
    <|> parseParens mathExpr
    <|> parseParens negateExpr
    <|> parseParens ifExpr
    <|> parseParens letExpr
    <|> parseParens lambdaExpr
    <|> parseParens applyExpr


-- a helper function for testing parsing
--   To use simply type:
--      parseString "5" 
--   this will call parseExpr
parseString :: String -> Either ErrorType (Expr, String) 
parseString str = do 
    parse parseExpr str



-- TODO: Add the following to MiniRacketParser.hs
-- Beginning of additions to MiniRacketParser.hs for Part 2 of the
--   MiniRacketProject
negateExpr :: Parser Expr
negateExpr = do
    symbol "-"
    NegateExpr <$> parseExpr


-- TODO: Implement negateAtom
-- negate an atom, we actually only have one choice here. Our
-- parsing already correctly handles negative numbers, and we
-- cannot have negative boolean values. This leaves variables, 
-- but this needs to build a NegateExpr around the VarExpr.
negateAtom :: Parser Expr
-- negateAtom = failParse "not implemented"
negateAtom = do
    symbol "-"
    NegateExpr <$> parseExpr


-- TODO: Implement varExpr
-- parse a var expression, here we need to make sure that
-- the identifier is *not* a keyword before accepting it
-- i.e., we fail the parse if it is     
varExpr :: Parser Expr
-- varExpr = failParse "not implemented"
varExpr = do
    fIdentifier <- identifier
    if fIdentifier `elem` keywordList then failParse "var not found" else return (VarExpr fIdentifier)

-- TODO: Implement ifExpr
-- parse an if-expression, which begins with the keyword if,
-- and is followed by three expressions
ifExpr :: Parser Expr
-- ifExpr = failParse "not implemented"
ifExpr = do
    parseKeyword "if"
    IfExpr <$> parseExpr <*> parseExpr <*> parseExpr


-- TODO: Implement let expressions  
-- a let expression begins with the keyword let, followed by
-- left parenthesis, then an identifier for the name 
-- to be bound, an expression to bind to that name, and a right
-- parenthesis, and then the body of the let expression
letExpr :: Parser Expr
-- letExpr = failParse "not implemented"
letExpr = do
    parseKeyword "let"
    symbol "("
    varId <- identifier
    expression <- parseExpr
    symbol ")"
    LetExpr varId expression <$> parseExpr

-- TODO: Implement lambdaExpr 
-- parse a lambda expression which is a lambda, argument, 
-- and body, with proper parenthesis around it
lambdaExpr :: Parser Expr
-- lambdaExpr = failParse "not implemented"
lambdaExpr = do
    parseKeyword "lambda"
    symbol "("
    varId <- kplus identifier
    symbol ")"
    expression <- parseExpr
    return (foldr LambdaExpr expression varId)
    


--TODO: Implement applyExpr
-- This expression consists of a function which is being applied to 
--   a parameter expression.
applyExpr :: Parser Expr
-- applyExpr = failParse "not implemented"
applyExpr = do
    expression <- kplus parseExpr
    return (foldl1 ApplyExpr expression)

-- End of additions to MiniRacketParser.hs for Part 2 of the
--   MiniRacketProject



