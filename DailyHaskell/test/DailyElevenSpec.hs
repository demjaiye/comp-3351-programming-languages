module DailyElevenSpec where

import Test.Hspec ( hspec, context, describe, it, shouldBe, Spec )
import DailyEleven (allLefts, produceStringOrSum, sumListOfEither)
main :: IO ()
main = hspec spec

spec :: Spec
spec = do

    describe "allLefts function" $ do
        it "should extract Left values from a list of Either values" $
            allLefts [Left "Error1", Right 20, Left "Error2", Right 34] `shouldBe` ["Error1", "Error2"]
        it "should return an empty list for a list with only Right values" $
            allLefts [Right 1, Right 2, Right 3] `shouldBe` ([] :: [String])
        it "should handle an empty input list" $
            allLefts [] `shouldBe` ([] :: [String])

    describe "produceStringOrSum function" $ do
        it "should produce String for Left input" $
            produceStringOrSum (Left "Hello") (Right 20) `shouldBe` Left "Hello"
        it "should produce Sum for Right input" $
            produceStringOrSum (Right 12) (Right 9) `shouldBe` Right 21
        it "should produce String if both inputs are Strings" $
            produceStringOrSum (Left "Error1") (Left "Error2") `shouldBe` Left "Error1"

    describe "sumListOfEither function" $ do
        it "should produce the first String from the list" $
            sumListOfEither [Left "Error", Right 10, Right 20] `shouldBe` Left "Error"
        it "should produce the sum of integers if there are no Strings" $
            sumListOfEither [Right 1, Right 2, Right 3] `shouldBe` Right 6
        it "should handle an empty list" $
            sumListOfEither [] `shouldBe` Right 0