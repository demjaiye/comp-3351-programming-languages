module WeeklyHaskellOneSpec where
import WeeklyHaskellOne
    ( removeChar,
      removeWhitespace,
      removePunctuation,
      charsToAscii,
      asciiToChars,
      shiftInts,
      shiftMessage,
      compositionalShift,
      removeAllChars )
import Test.Hspec ( hspec, context, describe, it, shouldBe, Spec )

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "removeChar" $ do
    context "removeChar 'l' 'Hello world'" $
      it "should be 'Heo word'" $
        (removeChar 'l' "Hello world") `shouldBe` "Heo word"

    context "removeChar ' ' 'Hello world'" $
      it "should be 'Helloworld'" $
        (removeChar ' ' "Hello world") `shouldBe` "Helloworld"
        
    context "removeChar '1' '12345' " $
      it "should be '2345' " $
        (removeChar '1' "12345") `shouldBe` "2345"     

  describe "removeWhitespace" $ do
    context "removeWhitespace 'Hello world'" $
      it "should be 'Helloworld'" $
        (removeWhitespace "Hello world") `shouldBe` "Helloworld"

    context "removeWhitespace ' \t\n\r '" $
      it "should be '' " $
        (removeWhitespace " \t\n\r ") `shouldBe` ""    

    context "removeWhitespace 'Demi\nis\ta\tgirl'" $
      it "should be Demiisagirl " $
        (removeWhitespace "Demi\nis\ta\tgirl") `shouldBe` "Demiisagirl"    

  describe "removePunctuation" $ do
    context "removePunctuation 'Demi, Dara, David.'" $
      it "should be 'Demi Dara David'" $
        (removePunctuation "Demi, Dara, David.") `shouldBe` "Demi Dara David"

    context "removePunctuation '[(String)]'" $
      it "should be 'String'" $
        (removePunctuation "[(String)]") `shouldBe` "String"

    context "removePunctuation '{{String}}'" $
      it "should be 'String'" $
        (removePunctuation "{{String}}") `shouldBe` "String"

  describe "charsToAscii" $ do
    context "charsToAscii 'Hello world!'" $
      it "should be '[72,101,108,108,111,32,119,111,114,108,100,33]'" $
        (charsToAscii "Hello world!") `shouldBe` [72,101,108,108,111,32,119,111,114,108,100,33]

    context "charsToAscii '' " $
      it "should be '[]'" $
        (charsToAscii "") `shouldBe` []

    context "charsToAscii 'A' " $
      it "should be '[65]'" $
        (charsToAscii "A") `shouldBe` [65]        

  describe "asciiToChars" $ do
    context "asciiToChars [65]" $
      it "should be 'A'" $
        (asciiToChars [65]) `shouldBe` "A"

    context "asciiToChars [97, 98, 99, 100]" $
      it "should be 'abcd'" $
        (asciiToChars [97, 98, 99, 100]) `shouldBe` "abcd"

    context "asciiToChars [72,101,108,108,111,32,119,111,114,108,100,33]" $
      it "should be Hello world!" $
        (asciiToChars [72,101,108,108,111,32,119,111,114,108,100,33]) `shouldBe` "Hello world!"


  describe "shiftInts" $ do
    context "shiftInts 10 [72, 101, 108, 108, 111, 44, 32, 87, 111, 114, 108, 100, 33]" $
      it "should be [82,111,118,118,121,54,42,97,121,124,118,110,43]" $
        (shiftInts 10 [72, 101, 108, 108, 111, 44, 32, 87, 111, 114, 108, 100, 33]) `shouldBe` [82,111,118,118,121,54,42,97,121,124,118,110,43]

    context "shiftInts 0 [72, 101, 108, 108, 111, 44, 32, 87, 111, 114, 108, 100, 33]" $
      it "should be [72,101,108,108,111,44,32,87,111,114,108,100,33]" $
        (shiftInts 0 [72, 101, 108, 108, 111, 44, 32, 87, 111, 114, 108, 100, 33]) `shouldBe` [72,101,108,108,111,44,32,87,111,114,108,100,33]

    context "shiftInts (-5) [65,97,120]" $
      it "should be [60,92,115]" $
        (shiftInts (-5) [65,97,120]) `shouldBe` [60,92,115]


  describe "shiftMessage" $ do
    context "shiftMessage 0 'Hello'" $
      it "should be 'Hello'" $
        (shiftMessage 0 "Hello") `shouldBe` "Hello"

    context "shiftMessage (-5) 'Hello'" $
      it "should be 'C`ggj'" $
        (shiftMessage (-5) "Hello") `shouldBe` "C`ggj"

    context "shiftMessage 1 'Hello'" $
      it "should be 'Ifmmp'" $
        (shiftMessage 1 "Hello") `shouldBe` "Ifmmp"

  describe "compositionalShift" $ do
    context "compositionalShift 0 'Hello'" $
      it "should be 'Hello'" $
        (compositionalShift 0 "Hello") `shouldBe` "Hello"

    context "compositionalShift (-5) 'Hello'" $
      it "should be 'C`ggj'" $
        (compositionalShift (-5) "Hello") `shouldBe` "C`ggj"

    context "compositionalShift 130 'abc'" $
      it "should be 'cde'" $
        (compositionalShift 130 "abc") `shouldBe` "cde"

  describe "removeAllChars" $ do
    context "removeAllChars [] 'Hello'" $
      it "should be 'Hello'" $
        (removeAllChars [] "Hello") `shouldBe` "Hello"

    context "removeAllChars ['a', 'b', 'c'] '' " $
      it "should be '' " $
        (removeAllChars ['a', 'b', 'c'] "") `shouldBe` ""

    context "removeAllChars [] '' " $
      it "should be '' " $
        (removeAllChars [] "") `shouldBe` ""
