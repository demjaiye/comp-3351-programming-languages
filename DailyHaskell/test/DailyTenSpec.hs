module DailyTenSpec where

import Data.Maybe (isJust, fromJust)
import Data.Char (isAlpha, chr)
import Test.Hspec ( hspec, context, describe, it, shouldBe, Spec )
import DailyTen( firstFunctorLaw, secondFunctorLaw, )
main :: IO ()
main = hspec spec

spec :: Spec
spec = do

    describe "First Functor Law" $ do
        context "Testing with Just ('c', 35)" $
            it "should satisfy the first functor law" $
                firstFunctorLaw (Just ('c', 35)) `shouldBe` True
        
        context "Testing with [2, 3, 5, 7, 11]" $
            it "should satisfy the first functor law" $
                firstFunctorLaw [2, 3, 5, 7, 11] `shouldBe` True

    describe "Second Functor Law" $ do
        context "Testing with Just ('c', 35)" $
            it "should satisfy the second functor law" $
                secondFunctorLaw isAlpha fst (Just ('c', 35)) `shouldBe` True
        
        context "Testing with [2, 3, 5, 7, 11]" $
            it "should satisfy the second functor law" $
                secondFunctorLaw chr (+ 96) [2, 3, 5, 7, 11] `shouldBe` True
                

    describe "Functor laws for Either String (Maybe Integer)" $ do
        context "First Functor Law" $ do
            it "should satisfy the first functor law for Left \"Error\" " $
                firstFunctorLaw (Left "Error" :: Either String (Maybe Integer)) `shouldBe` True

            it "should satisfy the first functor law for MyRight (Just 20)" $
                let myRightValue = fmap id (Right (Just 20)) :: Either String (Maybe Integer)
                in myRightValue `shouldBe` (Right (Just 20) :: Either String (Maybe Integer))

            it "should satisfy the first functor law for Right Nothing" $ do
                let myRightValue = fmap id (Right Nothing) :: Either String (Maybe Integer)
                myRightValue `shouldBe` (Right Nothing :: Either String (Maybe Integer))


        context "Second Functor Law" $ do
            it "should partially satisfy the second functor law for Left \"Error\"" $ do
                let myLeftValue = Left "Error" :: Either String (Maybe Integer)
                let f x = fmap (+1) x
                let g = fmap show
                let expected = (fmap g . fmap f) myLeftValue
                let actual = fmap (g . f) myLeftValue
                actual `shouldBe` expected

            it "should satisfy the second functor law for Right (Just 20)" $ do
                let myRightValue = Right (Just 20) :: Either String (Maybe Integer)
                let f x = fmap (+1) x
                let g = fmap show
                let expected = (fmap g . fmap f) myRightValue
                let actual = fmap (g . f) myRightValue
                actual `shouldBe` expected
            
            it "should satisfy the second functor law for Right Nothing" $ do
                let myRightValue = Right Nothing :: Either String (Maybe Integer)
                let f x = fmap (*2) x
                let g = fmap show
                let expected = (fmap g . fmap f) myRightValue
                let actual = fmap (g . f) myRightValue
                actual `shouldBe` expected




