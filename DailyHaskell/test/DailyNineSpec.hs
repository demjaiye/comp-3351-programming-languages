module DailyNineSpec where

import Test.Hspec ( hspec, context, describe, it, shouldBe, Spec )
import DailyNine (findSmallest, allTrue, countAllVotes)
main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "findSmallest" $ do
        context "findSmallest []" $
            it "should return Nothing" $
                (findSmallest [] :: Maybe Int) `shouldBe` Nothing

        context "findSmallest [20]" $
            it "should return Just x, where x is the single element" $
                (findSmallest [20]) `shouldBe` Just 20

        context "findSmallest [5, 2, 9, -1, 7]" $
            it "should return Just x, where x is the smallest element in the list" $
                (findSmallest [5, 2, 9, -1, 7]) `shouldBe` Just (-1)

    describe "allTrue" $ do
        context "allTrue []" $
            it "should be Nothing" $
                (allTrue []) `shouldBe` Nothing
        
        context "allTrue [True, False, True]" $
            it "should be Just False" $
                (allTrue [True, False, True]) `shouldBe` Just False

        context "allTrue [True, True, True]" $
            it "should be Just True" $
                (allTrue [True, True, True]) `shouldBe` Just True

    describe "countAllVotes" $ do
        context "countAllVotes []" $
            it "should return (0, 0, 0)" $
                (countAllVotes []) `shouldBe` (0, 0, 0)

        context "countAllVotes [Just True, Just True, Just True]" $
            it "should increment the first element of the tuple" $
                (countAllVotes [Just True, Just True, Just True]) `shouldBe` (0, 3, 0)

        context "countAllVotes [Just True, Just False, Nothing, Just True, Nothing, Just True]" $
            it "should increment corresponding elements of the tuple" $
                (countAllVotes [Just True, Just False, Nothing, Just True, Nothing, Just True]) `shouldBe` (2, 3, 1)        

