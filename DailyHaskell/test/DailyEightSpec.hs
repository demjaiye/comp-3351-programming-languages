module DailyEightSpec where

import Test.Hspec ( hspec, context, describe, it, shouldBe, Spec )
import DailyEight (events, inYear, inDayRange, inArea)
main :: IO ()
main = hspec spec

spec :: Spec
spec = do


    describe "inYear" $ do
        context "Empty event list" $
            it "returns an empty list" $
                inYear 1990 [] `shouldBe` []
        context "Events in the given year" $
            it "returns events occurred in the specified year" $
                inYear 2020 events `shouldBe` [("Event 3", 22, "Mar", 2020, 3, 3), ("Event 8", 20, "Aug", 2020, 8, 8), ("Event 9", 2, "Apr", 2020, 9, 9)]
        context "Events not in the given year" $
            it "returns an empty list" $
                inYear 2015 events `shouldBe` []

    describe "inDayRange" $ do
        context "Empty event list" $
            it "returns an empty list" $
                inDayRange 1 31 [] `shouldBe` []
        context "Events within the range" $
            it "returns events occurred within the specified days" $
                inDayRange 1 10 events `shouldBe` [("Event 1", 1, "Jan", 1990, 1, 1), ("Event 2",5,"Feb",1991,2.0,2.0), ("Event 5", 2, "May", 2000, 5, 5), ("Event 9",2,"Apr",2020,9.0,9.0)]
        context "No events within the range" $
            it "returns an empty list" $
                inDayRange 24 26 events `shouldBe` []

    describe "inArea" $ do
        context "Empty event list" $
            it "returns an empty list" $
                inArea "Event 1" 1 2 1 2 [] `shouldBe` []
        context "Events within the area" $
            it "returns events occurred within the specified area" $
                inArea "Event 3" 2 4 2 3 events `shouldBe` [("Event 3", 22, "Mar", 2020, 3, 3)]
        context "No events within the area" $
            it "returns an empty list" $
                inArea "Event 6" 1 5 1 5 events `shouldBe` []