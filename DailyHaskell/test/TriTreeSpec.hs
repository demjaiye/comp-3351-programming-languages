module TriTreeSpec where

import Test.Hspec ( hspec, context, describe, it, shouldBe, Spec )
import TriTree (TriTree(..), search, insert, insertList, identical, treeMap, treeFoldPreOrder, treeFoldInOrder, treeFoldPostOrder, delete, buildTree)
main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "search" $ do
        context "Empty tree" $
            it "returns False" $
                search 12 Empty `shouldBe` False

        context "Single-node tree (Leaf)" $ do
            it "returns True if target is the leaf value" $
                search 36 (Leaf 36) `shouldBe` True    

        context "Two-node tree (Node)" $ do
            it "returns True if target is in the node's values" $
                search 26 (Node 26 87 Empty Empty Empty) `shouldBe` True

    describe "insert" $ do
        context "Empty tree" $
            it "inserts value as a Leaf" $
                insert 42 Empty `shouldBe` Leaf 42

        context "Insert into a Leaf node" $ do
            it "inserts smaller value, creating a Node" $
                insert 10 (Leaf 20) `shouldBe` Node 10 20 (Leaf 10) (Leaf 20) Empty
    
        context "Insert into Leaf with larger value" $
            it "creates a Node with two values in ascending order and three empty subtrees" $
                insert 15 (Leaf 20) `shouldBe` Node 15 20 (Leaf 15) (Leaf 20) Empty

    describe "insertList" $ do
        context "Empty input list" $
            it "returns an empty TriTree" $
                (insertList [] Empty :: TriTree Int) `shouldBe` (Empty :: TriTree Int)

        context "Single element input list" $
            it "returns a TriTree with a single Leaf node" $ do
                insertList [1] Empty `shouldBe` Leaf 1

        context "Single element input list" $
            it "returns a TriTree with a single Leaf node" $ do
                insertList [32] Empty `shouldBe` Leaf 32

    describe "identical" $ do
        context "Empty trees" $
            it "returns True" $
                (identical (Empty :: TriTree Int) (Empty :: TriTree Int)) `shouldBe` True
        
        context "Empty and non-empty trees" $
            it "returns False" $
                identical Empty (Leaf 1) `shouldBe` False
    
        context "Leaf nodes with different values" $
            it "returns False" $
                identical (Leaf 1) (Leaf 2) `shouldBe` False
    
    describe "treeMap" $ do
        context "Empty tree" $
            it "returns Empty tree" $
                treeMap (\x -> x * 2) Empty `shouldBe` (Empty :: TriTree Int)

        context "Tree with a single Leaf" $
            it "applies the function to the Leaf value" $
                treeMap (\x -> x * 2) (Leaf 5) `shouldBe` (Leaf 10 :: TriTree Int)

        context "Tree with multiple Nodes and Leaves" $
            it "applies the function to all values in the tree" $ do
                let inputTree = Node 1 3 (Leaf 1) (Leaf 2) (Node 3 4 (Leaf 3) (Leaf 4) Empty)
                let expectedTree = Node 2 6 (Leaf 2) (Leaf 4) (Node 6 8 (Leaf 6) (Leaf 8) Empty)
                treeMap (\x -> x * 2) inputTree `shouldBe` expectedTree

    describe "treeFoldPreOrder" $ do
        context "Empty tree" $
            it "returns the initial accumulator value" $
                treeFoldPreOrder (\_ _ -> 0) 0 Empty `shouldBe` 0

        context "Tree with a single Leaf node" $
            it "applies the function to the accumulator and the leaf value" $
                treeFoldPreOrder (\acc val -> acc + val) 10 (Leaf 5) `shouldBe` 15

        context "Tree with multiple nodes in pre-order sequence" $
            it "applies the function to nodes in pre-order and returns the final accumulator value" $
                treeFoldPreOrder (\acc val -> acc * val) 1 (Node 1 2 (Leaf 2) (Leaf 3) (Leaf 4)) `shouldBe` 48
   
    describe "treeFoldInOrder" $ do
        context "Empty tree" $
            it "returns the initial accumulator" $
                treeFoldInOrder (\_ _ -> error "Should not be called") 0 Empty `shouldBe` 0

        context "Tree with a single Leaf" $
            it "applies the folding function to the Leaf value" $
                treeFoldInOrder (+) 1 (Leaf 5) `shouldBe` 6

        context "Tree with a single Node" $
            it "applies the folding function to the Node values" $
                treeFoldInOrder (*) 1 (Node 2 3 (Leaf 1) (Leaf 4) Empty) `shouldBe` 24

    describe "treeFoldPostOrder" $ do
        context "Empty tree" $
            it "returns the initial accumulator" $
                treeFoldPostOrder (\_ _ -> 1) 0 Empty `shouldBe` 0

        context "Tree with a single Leaf" $
            it "applies the folding function to the Leaf value and accumulator" $
                treeFoldPostOrder (\acc val -> acc + val) 0 (Leaf 5) `shouldBe` 5

        context "Tree with multiple Leaves" $
            it "applies the folding function to all Leaf values and accumulator" $
                treeFoldPostOrder (\acc val -> acc * val) 1 (Node 2 3 (Leaf 1) (Leaf 4) (Leaf 2)) `shouldBe` 48

    describe "delete" $ do
        context "Empty tree" $
            it "returns an empty tree" $
                delete 42 Empty `shouldBe` Empty

        context "Tree with single Leaf node" $ do
            it "returns an empty tree if the value matches" $
                delete 11 (Leaf 11) `shouldBe` Empty

        context "Tree with nodes" $ do
            it "removes the specified value from the tree" $
                delete 25 (Node 10 20 (Leaf 5) (Leaf 15) (Leaf 25)) `shouldBe` Node 10 20 (Leaf 5) (Leaf 15) Empty 

    describe "buildTree" $ do
        context "Empty input list" $
            it "returns an Empty tree" $
                (buildTree [] :: TriTree Int) `shouldBe` Empty

        context "Single element input list" $
            it "returns a Leaf with the single value" $
                buildTree [55] `shouldBe` Leaf 55

        context "Single element input list" $
            it "returns a Leaf with the single value" $
                buildTree [1] `shouldBe` Leaf 1
