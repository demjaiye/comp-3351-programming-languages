module DailyTwoSpec where

import Test.Hspec ( hspec, context, describe, it, shouldBe, Spec )
import DailyTwo (every4th, tupleDotQuotient, appendToEach, toSetList )
main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "every4th" $ do
    -- context "every4th []" $
    --   it "should be []" $
    --     (every4th []) `shouldBe` []    

    context "every4th [1]" $
      it "should be []" $
        (every4th [1]) `shouldBe` []   

    context "every4th [1,2,3]" $
      it "should be []" $
        (every4th [1,2,3]) `shouldBe` []                
    
    context "every4th [1,2,3,4]" $
      it "should be [4]" $
        (every4th [1,2,3,4]) `shouldBe` [4] 

    context "every4th [1,2,3,4,5,6,7,8]" $
      it "should be [4,8]" $
        (every4th [1,2,3,4,5,6,7,8]) `shouldBe` [4,8] 

  describe "tupleDotQuotient" $ do
    context "tupleDotQuotient [] []" $
      it "should be 0.0" $
        (tupleDotQuotient [][]) `shouldBe` 0.0

    context "tupleDotQuotient [2.5,6.0] [2.0,3.0]" $
      it "should be 3.25" $
        (tupleDotQuotient [2.5,6.0] [2.0,3.0]) `shouldBe` 3.25
    
    context "tupleDotQuotient [3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20] [3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]" $
      it "should be 18.0" $
        (tupleDotQuotient [3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20] [3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]) `shouldBe` 18.0

  describe "appendToEach" $ do
    context "appendToEach '!!!' []" $
      it "should be []" $
        (appendToEach "!!!"[]) `shouldBe` []

    context "appendToEach '' ['Hello', 'World']" $
      it "should be ['Hello', 'World']" $
        (appendToEach "" ["Hello", "World"]) `shouldBe` ["Hello", "World"]

    context "appendToEach '!!!' ['Hello']" $
      it "should be ['Hello!!!']" $
        (appendToEach "!!!" ["Hello"]) `shouldBe` ["Hello!!!"]  

    context "appendToEach '!!!' ['','']" $
      it "should be ['!!!', '!!!']" $
        (appendToEach "!!!" ["", ""]) `shouldBe` ["!!!", "!!!"]  

  describe "toSetList" $ do
    -- context "toSetList []" $
    --   it "should be []" $
    --     (toSetList []) `shouldBe` []  

    context "toSetList [1]" $
      it "should be [1]" $
        (toSetList [1]) `shouldBe` [1]  

    context "toSetList [1,1,1,2,2,2,3,3,3]" $
      it "should be [1,2,3]" $
        (toSetList [1,1,1,2,2,2,3,3,3]) `shouldBe` [1,2,3]          

    context "toSetList [1,2,3,4]" $
      it "should be [1,2,3,4]" $
        (toSetList [1,2,3,4]) `shouldBe` [1,2,3,4]               