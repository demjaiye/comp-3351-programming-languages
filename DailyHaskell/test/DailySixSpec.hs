module DailySixSpec where

import Test.Hspec ( hspec, context, describe, it, shouldBe, Spec )
import DailySix (shorterThan, removeMultiples, onlyJust, allAnswers)
main :: IO ()
main = hspec spec

spec :: Spec
spec = do    
  describe "shorterThan []" $ do
    context "shorterThan" $
      it "should return an empty list" $
        (shorterThan 5 []) `shouldBe` []

    context "shorterThan ['aunty', 'uncle', 'grandpa', 'grandma']" $
      it "should filter out words longer than x" $
        (shorterThan 5 ["aunty", "uncle", "grandpa", "grandma"]) `shouldBe` ["aunty", "uncle"]

    context "shorterThan ['aunty', 'uncle', 'grandpa', 'grandma']" $
      it "should return an empty list" $
        (shorterThan 3 ["aunty", "uncle", "grandpa", "grandma"]) `shouldBe` []

  describe "removeMultiples" $ do
    context "removeMultiples 2 [1, 2, 3, 4, 5, 6]" $
      it "should return [1,3,5]" $
        (removeMultiples 2 [1, 2, 3, 4, 5, 6]) `shouldBe` [1, 3, 5]

    context "removeMultiples 1 [1, 2, 3, 4, 5, 6]" $
      it "should return []" $
        (removeMultiples 1 [1, 2, 3, 4, 5, 6]) `shouldBe` []
        
    context "removeMultiples 3 [-3, - 4, -6]" $
      it "should return []" $
        (removeMultiples 3 [-3, -6, -9]) `shouldBe` []

  describe "onlyJust" $ do
    context "onlyJust []" $
      it "should return an empty list" $
        (onlyJust ([] :: [Maybe Int])) `shouldBe` []

    context "onlyJust [Just 1, Just 2, Just 3]" $
      it "should return the same list" $
        (onlyJust [Just 1, Just 2, Just 3]) `shouldBe` [Just 1, Just 2, Just 3]

    context "onlyJust [Just 1, Nothing, Just 2, Nothing, Just 3]" $
      it "should filter out the Nothing values and return a list of Just values only" $
        (onlyJust [Just 1, Nothing, Just 2, Nothing, Just 3]) `shouldBe` [Just 1, Just 2, Just 3]

  describe "allAnswers" $ do
    context "empty list)" $
      it "should return an empty list wrapped in Just" $
        (allAnswers (\x -> Just (x * 2)) []) `shouldBe` Just []

    context "function returns Nothing)" $
      it "should return Nothing if any call of the function returns Nothing" $
        (allAnswers (\x -> if x `mod` 2 == 0 then Just (x * 2) else Nothing) [1, 2, 3]) `shouldBe` Nothing

    context "function always returns Just" $
      it "should return a list of values wrapped in Just" $
        (allAnswers (\x -> Just (x * 2)) [1, 2, 3]) `shouldBe` Just [2, 4, 6]