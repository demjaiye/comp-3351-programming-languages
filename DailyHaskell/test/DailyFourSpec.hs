module DailyFourSpec where

import Test.Hspec ( hspec, context, describe, it, shouldBe, Spec )
import DailyFour (zip3Lists, unzipTriples, mergeSorted3)

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "zip3Lists" $ do
    context "zip3Lists [][][]" $
      it "should be []" $
        (zip3Lists ([] :: [Int]) ([] :: [Char]) ([] :: [Bool])) `shouldBe` ([] :: [(Int, Char, Bool)])

    context "zip3Lists [2,3,4,5]['a','b','c','d'][1,2,3,4]" $
      it "should be [(2,'a',1),(3,'b',2),(4,'c',3),(5,'d',4)]" $
        (zip3Lists [2,3,4,5]['a','b','c','d'][1,2,3,4]) `shouldBe` [(2,'a',1),(3,'b',2),(4,'c',3),(5,'d',4)]

    context "zip3Lists ['yes'] [True] [1]" $
      it "should be [('yes',True,1)]" $
        (zip3Lists ["yes"] [True] [1]) `shouldBe` [("yes",True,1)]

  describe "unzipTriples" $ do
    context "unzipTriples []" $
      it "should be ([],[],[])" $
         (unzipTriples ([] :: [(Char, Int, Bool)])) `shouldBe` ([],[],[])

    context "unzipTriples [( 'A', 1, True)]" $
      it "should be ('A',[1],[True])" $
        (unzipTriples [( 'A', 1, True)]) `shouldBe` ("A",[1],[True])

    context "unzipTriples [ (1,2,3), (4,5,6), (7,8,9) ] " $
      it "should be ([1,4,7],[2,5,8],[3,6,9])" $
        (unzipTriples [(1,2,3), (4,5,6), (7,8,9) ]) `shouldBe` ([1,4,7],[2,5,8],[3,6,9])

  describe "mergeSorted3" $ do
    context "mergeSorted3 [] [] []" $
      it "should be []" $
        (mergeSorted3 [] [] []) `shouldBe` ([] :: [Int])

    context "mergeSorted3 [] [1, 2, 3] [4, 5, 6]" $
      it "should be [1, 2, 3, 4, 5, 6]" $
        (mergeSorted3 [] [1, 2, 3] [4, 5, 6]) `shouldBe` [1, 2, 3, 4, 5, 6]

    context "mergeSorted3 [2] [3] [1]" $
      it "should be [1, 2, 3]" $
        (mergeSorted3 [2] [3] [1]) `shouldBe` [1, 2, 3]               


