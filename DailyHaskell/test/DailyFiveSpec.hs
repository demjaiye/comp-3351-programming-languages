module DailyFiveSpec where

import Test.Hspec ( hspec, context, describe, it, shouldBe, Spec )
import DailyFive (multPairs, squareList, findLowercase, compositionMap)
main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "multPairs" $ do
        context "multPairs []" $
            it "should be []" $
            (multPairs []) `shouldBe` []

        context "multPairs (1, 2)" $
            it "should be [2]" $
            (multPairs [(1, 2)]) `shouldBe` [2]

        context "multPairs [(-1, 6), (-2, 6), (-4, 5)]" $
            it "should multiply pairs and return [-6, -12, -20]" $
            (multPairs [(-1, 6), (-2, 6), (-4, 5)]) `shouldBe` [-6, -12, -20]


    describe "squareList" $ do
        context "squareList []" $
            it "should return an empty list" $
            (squareList []) `shouldBe` []

        context "squareList [1, 2, 3, 4, 5]" $
            it "should return a list of tuples containing each integer and its square" $
            (squareList [1, 2, 3, 4, 5]) `shouldBe` [(1, 1), (2, 4), (3, 9), (4, 16), (5, 25)]

        context "squareList[-1, -2, -3, -4, -5]" $
            it "should return a list of tuples containing each integer and its square" $
            (squareList [-1, -2, -3, -4, -5]) `shouldBe` [( -1, 1), ( -2, 4), ( -3, 9), ( -4, 16), ( -5, 25)]


    describe "findLowercase" $ do
        context "findLowercase []" $
            it "should return an empty list" $
            (findLowercase []) `shouldBe` []

        context "findLowercase ['HELLO', 'WORLD', 'HASKELL']" $
            it "should return a list of all False" $
            (findLowercase ["HELLO", "WORLD", "HASKELL"]) `shouldBe` [False, False, False]

        context "findLowercase ['hello', 'world', 'haskell']" $
            it "should return a list of all True" $
            (findLowercase ["hello", "world", "haskell"]) `shouldBe` [True, True, True]


    describe "compositionMap" $ do
        context "compositionMap []" $
            it "should return an empty list" $
            (compositionMap []) `shouldBe` []

        context "compositionMap [1.0, 2.0, 3.0, 4.0, 5.0]" $
            it "should apply the composition function correctly" $
            (compositionMap [1.0, 2.0, 3.0, 4.0, 5.0]) `shouldBe` [-0.5998406,0.44195887,1.5236518,0.7659697,-0.29152355]

        context "compositionMap [-1.0, -2.0, -3.0, -4.0, -5.0]" $
            it "should apply the composition function correctly" $
            (compositionMap [-1.0, -2.0, -3.0, -4.0, -5.0]) `shouldBe` [-0.5998406,0.44195887,1.5236518,0.7659697,-0.29152355]
