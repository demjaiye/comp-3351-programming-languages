
module DailyOneSpec where

import Test.Hspec ( hspec, context, describe, it, shouldBe, Spec )
import DailyOne ( quadratic, scaleVector, tripleDistance )

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "quadratic" $ do
    context "quadratic 0 0 0 1" $
      it "should be 0" $
        (quadratic 0 0 0 1) `shouldBe` 0

    context "quadratic 0 0 1 0" $
      it "should be 0" $
        (quadratic 0 0 1 0) `shouldBe` 0

    context "quadratic 0 1 0 0" $
      it "should be 0" $
        (quadratic 0 1 0 0) `shouldBe` 0

    context "quadratic 1 0 0 0" $
      it "should be 1" $
        (quadratic 1 0 0 0) `shouldBe` 1

    context "quadratic 5 3 2 1" $
      it "should be 10" $
        (quadratic 5 3 2 1) `shouldBe` 10

    context "quadratic 1 2 3 4" $
      it "should be 57" $
        (quadratic 1 2 3 4) `shouldBe` 57

    context "quadratic 1 0 1 0" $
      it "should be 1" $
        (quadratic 1 0 1 0) `shouldBe` 1



  describe "scaleVector" $ do
    context "scaleVector 5 (1,0)" $
      it "should be (5, 0)" $
        (scaleVector 5 (1, 0)) `shouldBe` (5, 0)

    context "scaleVector 10 (0,1)" $
      it "should be (0, 10)" $
        (scaleVector 10 (0, 1)) `shouldBe` (0, 10)

    context "scaleVector 0 (1,1)" $
      it "should be (0, 0)" $
        (scaleVector 0 (0, 0)) `shouldBe` (0, 0)

    context "scaleVector 3 (2,3)" $
      it "should be (6, 9)" $
        (scaleVector 3 (2, 3)) `shouldBe` (6, 9)

    context "scaleVector 5 (3,4)" $
      it "should be (15, 20)" $
        (scaleVector 5 (3, 4)) `shouldBe` (15, 20)

    context "scaleVector 3 (1,1)" $
      it "should be (3, 3)" $
        (scaleVector 3 (1, 1)) `shouldBe` (3, 3)

    context "scaleVector 1 (0,1)" $
      it "should be (0, 1)" $
        (scaleVector 1 (0, 1)) `shouldBe` (0, 1)


  describe "tripleDistance" $ do
    context "tripleDistance (0,0,1) (0,0,0)" $
      it "should be 1.0" $
        (tripleDistance (0, 0, 1) (0, 0, 0)) `shouldBe` 1.0 

    context "tripleDistance (0,0,1) (0,0,-1)" $
      it "should be 2.0" $
        (tripleDistance (0, 0, 1) (0, 0, -1)) `shouldBe` 2.0 

    context "tripleDistance (0,0,1) (0,1,0)" $
      it "should be sqrt(2)" $
        (tripleDistance (0, 0, 1) (0, 1, 0)) `shouldBe` 
           (sqrt ((0 - 0)^2 + (0 - 1)^2 + (1 - 0)^2))

    context "tripleDistance (1,0,0) (0,0,1)" $
      it "should be 1.4142135623730951" $
        (tripleDistance (1, 0, 0) (0, 0, 1)) `shouldBe` 1.4142135623730951 

    context "tripleDistance (2,1,0) (0,0,0)" $
      it "should be 2.23606797749979" $
        (tripleDistance (2, 1, 0) (0, 0, 0)) `shouldBe` 2.23606797749979 

    context "tripleDistance (0,0,0) (4,9,16)" $
      it "should be 18.788294228055936" $
        (tripleDistance (0, 0, 0) (4, 9, 16)) `shouldBe` 18.788294228055936         