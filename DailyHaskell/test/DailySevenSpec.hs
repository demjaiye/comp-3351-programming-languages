module DailySevenSpec where

import Test.Hspec ( hspec, context, describe, it, shouldBe, Spec )
import DailySeven (findLongest, anyLarger, allNames)
main :: IO ()
main = hspec spec

spec :: Spec
spec = do
    describe "findLongest" $ do
        context "Empty input list" $
            it "returns an empty string" $
                findLongest [] `shouldBe` ""

        context "Input list with equal longest strings" $
            it "returns the first occurrence of the longest string" $
                findLongest ["apple", "kiwi", "mango", "grape"] `shouldBe` "apple"

        context "Input list with varying string lengths" $
            it "returns the longest string" $
                findLongest ["apple", "banana", "kiwi", "date"] `shouldBe` "banana" 


    describe "anyLarger" $ do
        context "when given an empty list" $
            it "should return False for any input number" $ do
                anyLarger 5 [] `shouldBe` False

        context "when the list contains a single element" $
            it "should return True if the element is greater than or equal to the input number, False otherwise" $ do
                anyLarger 5 [10] `shouldBe` True

        context "when the list contains multiple elements" $
            it "should return True if any element is greater than or equal to the input number, False otherwise" $ do
                anyLarger 5 [1, 3, 4, 2] `shouldBe` False      


    describe "allNames" $ do
        context "Empty input list" $
            it "should return an empty string" $
                (allNames []) `shouldBe` ""

        context "Input list with one non-empty tuple" $
            it "should concatenate the names correctly" $
                (allNames [("Kermit", "the frog")]) `shouldBe` "Kermit the frog"

        context "Input list with multiple tuples" $
            it "should concatenate the names with commas and spaces" $
                (allNames [("Kermit", "the frog"), ("bugs", "bunny")]) `shouldBe` "Kermit the frog, bugs bunny"
