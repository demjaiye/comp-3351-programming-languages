module WeeklyThreeSpec where

import Test.Hspec ( hspec, context, describe, it, shouldBe, Spec )
import WeeklyThree (Vec(..))

main :: IO ()
main = hspec spec

spec :: Spec
spec = do


    describe "Vec operations" $ do
        context "Empty vector" $
            it "shows an empty vector" $
                show (Vec []) `shouldBe` "Vec []"

        context "Non-empty vector" $
            it "shows a vector with elements" $
                show (Vec [1.0, 2.0, 3.0]) `shouldBe` "Vec [1.0,2.0,3.0]"

        context "Addition" $
            it "adds two vectors element-wise" $
                (Vec [1.0, 2.0, 3.0] + Vec [4.0, 5.0, 6.0]) `shouldBe` Vec [5.0, 7.0, 9.0]

        context "Subtraction" $
            it "subtracts two vectors element-wise" $
                (Vec [4.0, 5.0, 6.0] - Vec [1.0, 2.0, 3.0]) `shouldBe` Vec [3.0, 3.0, 3.0]

        context "Multiplication" $
            it "multiplies two vectors element-wise" $
                (Vec [1.0, 2.0, 3.0] * Vec [4.0, 5.0, 6.0]) `shouldBe` Vec [4.0, 10.0, 18.0]

        context "Empty vectors" $
            it "two empty vectors should be equal" $
                (Vec [] == Vec []) `shouldBe` True

        context "Vectors with the same elements" $
            it "vectors with the same elements should be equal" $
                (Vec [1.0, 2.0, 3.0] == Vec [1.0, 2.0, 3.0]) `shouldBe` True

        context "Equal vectors" $
            it "compares equal vectors and returns EQ" $
                (Vec [1.0, 2.0, 3.0] `compare` Vec [1.0, 2.0, 3.0]) `shouldBe` EQ

        context "Vectors with different lengths" $
            it "compares vectors with different lengths" $
                (Vec [1.0, 2.0, 3.0] `compare` Vec [1.0, 2.0]) `shouldBe` GT

        context "Semigroup operation" $
            it "performs semigroup operation using addition" $
                (Vec [1.0, 2.0] <> Vec [3.0, 4.0]) `shouldBe` Vec [4.0, 6.0]

        context "Empty vectors" $
            it "returns the second vector for the first vector being empty" $
                (Vec [] <> Vec [1.0, 2.0, 3.0]) `shouldBe` Vec [1.0, 2.0, 3.0]

        context "Vectors with equal lengths" $
            it "adds two vectors element-wise" $
                (Vec [1.0, 2.0, 3.0] <> Vec [4.0, 5.0, 6.0]) `shouldBe` Vec [5.0, 7.0, 9.0]

        context "Monoid identity element" $
            it "uses a vector of zeros as the identity element" $
                (Vec [1.0, 2.0, 3.0] `mappend` mempty) `shouldBe` Vec [1.0, 2.0, 3.0]

        context "Empty vector" $
            it "should return mempty when creating a Vec from an empty list" $
                (Vec [] `shouldBe` mempty)

        context "Non-empty vector" $
            it "should return the same Vec when creating a Vec from a non-empty list" $
                (Vec [1.0, 2.0, 3.0] `shouldBe` Vec [1.0, 2.0, 3.0])
