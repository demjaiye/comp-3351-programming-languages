module DailyThreeSpec where

import Test.Hspec ( hspec, context, describe, it, shouldBe, Spec )
import DailyThree ( removeAllExcept, countOccurrences, substitute, setListDiff )

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "removeAllExcept" $ do
    context "removeAllExcept 1 [2,3,4,5]" $
      it "should be []" $
        (removeAllExcept 1 [2,3,4,5]) `shouldBe` []

    context "removeAllExcept 1 []" $
      it "should be []" $
        (removeAllExcept 1 []) `shouldBe` []

    context "removeAllExcept 'a' ['a','b','c','a']" $
      it "should be 'aa'" $
        (removeAllExcept 'a' ['a','b','c','a']) `shouldBe` "aa"

  describe "countOccurrences" $ do
    context "countOccurrences 1 [2,4,6,3,2]" $
      it "should be 0" $
        (countOccurrences 1 [2,4,6,3,2]) `shouldBe` 0

    context "countOccurrences a ['a','b','c','a']" $
      it "should be 2" $
        (countOccurrences 'a' ['a','b','c','a']) `shouldBe` 2

    context "countOccurrences 'apple' ['apple', 'banana', 'apple', 'cherry']" $
      it "should be 2" $
        (countOccurrences "apple" ["apple", "banana", "apple", "cherry"]) `shouldBe` 2

  describe "substitute" $ do
    context "substitute 'a' 'b' 'hello'" $
      it "should be 'hello'" $
        (substitute 'a' 'b' "hello") `shouldBe` "hello"

    context "substitute 'a' 'b' 'aaa'" $
      it "should be 'bbb'" $
        (substitute 'a' 'b' "aaa") `shouldBe` "bbb"

    context "substitute 'hello' 'hi' ['hello', 'demi']" $
      it "should be ['hi','demi']" $
        (substitute "hello" "hi" ["hello", "demi"]) `shouldBe` ["hi","demi"]

  describe "setListDiff" $ do
    context "setListDiff [] [1,2,3]" $
      it "should be [1,2,3]" $
        (setListDiff [] [1,2,3]) `shouldBe` [1,2,3]


    context "setListDiff [1,2,3] [4,5,6]" $
      it "should be [4,5,6]" $
        (setListDiff [1,2,3] [4,5,6]) `shouldBe` [4,5,6]

    context "setListDiff [1,2,3] [1,2,3]" $
      it "should be []" $
        (setListDiff [1,2,3] [1,2,3]) `shouldBe` []
