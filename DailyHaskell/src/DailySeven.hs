module DailySeven where
import Data.List (intersperse)

-- Purpose: Finds the longest string from a list of words.
-- Parameters: List of strings ([String]).
-- Results: Longest string from the input list, or an empty string ("") if the input list is empty.
findLongest :: [String] -> String
findLongest words = foldl (\longest word -> if length longest >= length word then longest else word) "" words

-- Purpose: Checks if any element in the list is larger than or equal to the given number.
-- Parameters: An integer num and a list of integers xs.
-- Results: Returns True if any element in the list is greater than or equal to num, otherwise, False.
anyLarger :: Int -> [Int] -> Bool
anyLarger num xs = foldl (\acc x -> acc || x >= num) False xs

-- Purpose: Concatenates first names and last names from a list of tuples, separated by commas and spaces.
-- Parameters: List of tuples containing first names and last names.
-- Result: A single string containing concatenated names separated by commas.
allNames :: [(String,String)] -> String
allNames words = concat $ intersperse ", " [fName ++ " " ++ lName | (fName, lName) <- words]
