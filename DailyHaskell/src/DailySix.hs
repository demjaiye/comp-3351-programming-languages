module DailySix where

-- Purpose: The shorterThan function filters a list of strings, keeping only the strings whose length is less than or equal to a specified integer x.
-- Parameters: It takes an integer x and a list of strings words as input.
-- Result: The function returns a list of strings, containing only those strings from the input list that have a length less than or equal to the specified integer x.
shorterThan :: Int -> [String] -> [String]
shorterThan x words = filter (\ xs -> length xs <= x )words


-- Purpose: The removeMultiples function filters out elements from a list of integers that are multiples of a given integer x.
-- Parameters: The function takes two parameters: an integer x (the divisor) and a list of integers nums from which multiples of x will be removed.
-- Result: The function returns a list of integers where all the elements that are multiples of x have been removed.
removeMultiples :: Int -> [Int] -> [Int]
removeMultiples x nums = filter (\ num -> mod num x /= 0) nums


-- Purpose: The onlyJust function filters a list of Maybe a values, retaining only the elements that are Just, discarding the Nothing values.
-- Parameters: xs: A list of Maybe a values where a can be any data type.
-- Result: A list of Maybe a values containing only the Just elements from the input list, excluding the Nothing values.
onlyJust :: [Maybe a] -> [Maybe a]
onlyJust xs = filter(not . isNothing)xs
isNothing :: Maybe a -> Bool
isNothing Nothing = True
isNothing _ = False


-- Purpose: The allAnswers function takes a function f of type (a -> Maybe b) and a list xs of type [a]. It applies the function f to each element of the list. If all calls to f return Just b, the function collects the results into a list and returns Just this list. If any call of the function returns Nothing, the function returns Nothing.

-- Parameters: 
    -- f: A function that takes an element of type a and returns a Maybe value of type b.
    -- xs: A list of type [a] containing elements of type a to be processed by the function f.
-- Result: Returns a Maybe value containing a list of type [b]. If all calls to f return Just b, it returns Just the list of extracted b values. If any call of the function returns Nothing, the function returns Nothing.
allAnswers :: (a -> Maybe b) -> [a] -> Maybe [b]
allAnswers f xs = foldr anyCall (Just []) (map f xs)
  where
    anyCall :: Maybe b -> Maybe [b] -> Maybe [b]
    anyCall (Just x) (Just acc) = Just (x : acc)
    anyCall _ _ = Nothing

