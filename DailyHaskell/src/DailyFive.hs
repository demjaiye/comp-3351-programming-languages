module DailyFive where
import Data.Char

-- Purpose: The multPairs function takes a list of pairs of integers and calculates the product of each pair.
-- Parameters: multPairs accepts a list of tuples, where each tuple contains two integers.
-- Result: The function returns a list of integers, where each element represents the product of the corresponding pair in the input list of tuples.
multPairs :: [(Int, Int)] -> [Int]
multPairs = map (\ (x, y) -> x*y)

-- Purpose: The purpose of the squareList function is to take a list of integers as input and produce a list of tuples. Each tuple contains an integer from the input list and its square.
-- Parameters: The squareList function takes a list of integers [Int] as its input parameter.
-- Result: The squareList function returns a list of tuples [(Int, Int)], where each tuple consists of an integer from the input list and its corresponding square.
squareList :: [Int] -> [(Int, Int)]
squareList = map (\ x -> (x, x*x))

-- Purpose: The purpose of findLowercase is to determine whether the first character of each string in the input list is a lowercase letter and return a list of booleans indicating the result for each string.
-- Parameters: The findLowercase function takes a list of strings [String] as its input parameter.
--Result: The findLowercase function returns a list of booleans [Bool], where each boolean represents whether the first character of the corresponding string in the input list is a lowercase letter.
findLowercase :: [String] -> [Bool]
findLowercase = map (isLower . head) 

-- Purpose: The purpose of compositionMap is to apply a composition of functions (cosine, tangent, and negation) to each element in the input list and return a new list of the resulting values.
-- Parameters: The compositionMap function takes a list of Float numbers [Float] as its input parameter.
-- Result: The compositionMap function returns a list of Float numbers [Float], where each number in the output list is the result of applying the composition of negate . tan . cos to the corresponding number in the input list.
compositionMap :: [Float] -> [Float]
compositionMap = map (negate . tan . cos)