module DailyTwo where

-- Purpose: The 'every4th' function constructs a new list consisting of every fourth element
-- Function Parameters: 'list' is a list of elements of any type '[a]' from which every fourth element is extracted.
-- Function Result: The function returns a new list containing every fourth element from the input list.
every4th :: [a] -> [a]
every4th [] = []
every4th list = every4th' list 0
  where
    every4th' :: [a] -> Int -> [a]
    every4th' [] _ = [] 
    every4th' (x:xs) counter
        | counter == 3 = x : every4th' xs 0 
        | otherwise = every4th' xs (counter + 1) 

every4thFoldr :: [a] -> [a]
every4thFoldr = snd . foldr f (0, [])
  where
    f x (counter, acc)
      | counter == 0 = (3, x : acc)
      | otherwise    = (counter - 1, acc)



-- Purpose: The 'tupleDotQuotient' function calculates the dot quotient between two lists of numbers, and produces a single fractional value
-- Function Parameters: 'q' is a list of fractional numbers '[a]' representing the numerator vector.
--                      'p' is a list of fractional numbers '[a]' representing the denominator vector.
-- Function Result: The function returns a single fractional number 'a' as the dot quotient.
tupleDotQuotient :: (Fractional a) => [a] -> [a] -> a
tupleDotQuotient [] [] = 0  
tupleDotQuotient (q:qs) (p:ps) = (q / p) + tupleDotQuotient qs ps
tupleDotQuotient _ _ = error "Lists must be the same size."

tupleDotQuotientFoldr :: (Fractional a) => [a] -> [a] -> a
tupleDotQuotientFoldr [] [] = 0
tupleDotQuotientFoldr qList pList
  | length qList == length pList = foldr (\(q, p) acc -> acc + q / p) 0 (zip qList pList)
  | otherwise = error "Lists must be the same size."


-- Purpose: The 'appendToEach' function takes a string 'String' and a list of strings '[String]' and appends the string to each string in the list to produces a new list of strings '[String]'. 
-- Function Parameters: 'String' is a string that is appended to each element of the input list. 
--                      'xs' is a list of strings '[String]' to which 'String' is appended
-- Function Result: The function returns a new list of strings '[String]' where 'String' has been appended to each element of the input list
appendToEach :: String -> [String] -> [String]
appendToEach _ [] = [] 
appendToEach str (x:xs) = (x ++ str) : appendToEach str xs

appendToEachFoldr :: String -> [String] -> [String]
appendToEachFoldr str xs = foldr (\x acc -> (x ++ str) : acc) [] xs


-- Purpose: The 'toSetList' function converts a list '[a]' to a set representation, removing duplicate elements.
-- Function Parameters: 'xs' is a list of elements '[a]' from which duplicates are removed.
-- Function Result: The function returns a new list '[a]' with duplicate elements removed.
isInResult :: Eq a => a -> [a] -> Bool
isInResult _ [] = False
isInResult x (y:ys)
    | x == y    = True
    | otherwise = isInResult x ys

toSetList :: Eq a => [a] -> [a]
toSetList [] = [] 
toSetList (x:xs)
    | isInResult x xs = toSetList xs  
    | otherwise = x : toSetList xs

isInResultFoldr :: Eq a => a -> [a] -> Bool
isInResultFoldr x xs = foldr (\y acc -> x == y || acc) False xs

toSetListFoldr :: Eq a => [a] -> [a]
toSetListFoldr xs = foldr (\x acc -> if x `isInResult` acc then acc else x : acc) [] xs
