module DailyEight where

type Event = (String,Int,String,Int,Double,Double)

-- inYear function
-- Purpose: Filter events that occurred in a specific year.
-- Parameter: Int representing the target year, [Event] representing the list of events.
-- Result: [Event] containing events that happened in the specified year.
inYear :: Int -> [Event] -> [Event]
inYear year events = filter (\(_, _, _, eventYear, _, _) -> eventYear == year) events

-- inDayRange function
-- Purpose: Filter events that occurred within a specific range of days (inclusive).
-- Parameter: Two Int values representing the start and end day, [Event] representing the list of events.
-- Result: [Event] containing events that happened within the specified day range.
inDayRange :: Int -> Int -> [Event] -> [Event]
inDayRange startDay endDay events =  filter (\(_, day, _, _, _, _) -> day >= startDay && day <= endDay) events

-- inArea function
-- Purpose: Filter events based on a specific name and location area.
-- Parameter: String representing event name, Double values representing x and y coordinates for lower and upper bounds, [Event] representing the list of events.
-- Result: [Event] containing events with the specified name and falling within the specified region.
inArea :: String -> Double -> Double -> Double -> Double -> [Event] -> [Event]
inArea name lowerx upperx lowery uppery events = filter(\(eventName, _, _, _, x, y) -> eventName == name && (x >= lowerx && x <= upperx) && (y >= lowery && y <= uppery)) events

events :: [Event]
events = [
    ("Event 1", 1, "Jan", 1990, 1, 1),
    ("Event 2", 5, "Feb", 1991, 2, 2),
    ("Event 3", 22, "Mar", 2020, 3, 3),
    ("Event 4", 12, "Apr", 1998, 4, 4),
    ("Event 5", 2, "May", 2000, 5, 5),
    ("Event 6", 29, "Jun", 2005, 6, 6),
    ("Event 7", 31, "Jul", 2007, 7, 7),
    ("Event 8", 20, "Aug", 2020, 8, 8),
    ("Event 9", 2, "Apr", 2020, 9, 9)] 


