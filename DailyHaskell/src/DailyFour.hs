module DailyFour where

-- Purpose: The zip3Lists function takes three input lists and combines them element-wise into a list of triples.
-- Parameters: It accepts three lists of potentially different types: [a], [b], and [c].
-- Result: The result is a list of triples [(a, b, c)] where each element at the same position from the input lists is grouped together in a triple.
zip3Lists :: [a] -> [b] -> [c] -> [(a, b, c)]
zip3Lists [] [] [] = []
zip3Lists (x:xs)(y:ys)(z:zs) = (x, y, z) : zip3Lists xs ys zs
zip3Lists _ _ _ = error "Input lists must have the same length"

-- Purpose: The unzipTriples function takes a list of triples and produces a tuple of three lists. 
-- Parameters: The input is a list of triples [(a, b, c)].
-- Result: The function returns a tuple ([a], [b], [c]) containing three lists, where the first list contains all the first components of 
    -- the triples, the second list contains all the second components, and the third list contains all the third components.
unzipTriples :: [(a,b,c)] -> ([a], [b], [c])
unzipTriples [] = (([], [], []))
unzipTriples ((a,b,c):xs) =
    let 
        (as, bs, cs) = unzipTriples xs
    in
        (a:as, b:bs, c:cs)


-- Purpose: The mergeSorted3 function merges three sorted lists into a single sorted list.
-- Parameters: Three lists of type 'a' that are already sorted in ascending order.
-- Result: A single sorted list containing all elements from the input lists.
mergeSorted3 :: Ord a => [a] -> [a] -> [a] -> [a]
mergeSorted3 [] [] [] = []
mergeSorted3 xs [] [] = xs
mergeSorted3 [] ys [] = ys
mergeSorted3 [] [] zs = zs
mergeSorted3 xs ys [] = mergeSorted2 xs ys
mergeSorted3 xs [] zs = mergeSorted2 xs zs 
mergeSorted3 [] ys zs = mergeSorted2 ys zs  
mergeSorted3 (x:xs) (y:ys) (z:zs)
    | x <= y && x <= z = x : mergeSorted3 xs (y:ys) (z:zs)
    | y <= x && y <= z = y : mergeSorted3 (x:xs) ys (z:zs)
    | otherwise        = z : mergeSorted3 (x:xs) (y:ys) zs


mergeSorted2 :: Ord a => [a] -> [a] -> [a]
mergeSorted2 xs [] = xs
mergeSorted2 [] ys = ys
mergeSorted2 (x:xs) (y:ys)
    | x <= y    = x : mergeSorted2 xs (y:ys)
    | otherwise = y : mergeSorted2 (x:xs) ys

