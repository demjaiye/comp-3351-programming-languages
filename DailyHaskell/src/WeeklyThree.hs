module WeeklyThree where

import Data.Semigroup (Semigroup, (<>))
import Data.Monoid (Monoid, mempty, mappend)

-- Function Purpose: Define a custom data type Vec representing a vector of Double values.
-- Function Parameters: None.
-- Result: The Vec data type is defined, allowing the creation and manipulation of vectors with element-wise operations.

data Vec = Vec [Double]

instance Show Vec where
    show :: Vec -> String
    show (Vec values) = "Vec " ++ show values

-- Function Purpose: Implement basic mathematical operations (+, -, *, abs) for Vec instances.
-- Function Parameters: Two Vec instances for addition, subtraction, and multiplication. One Vec instance for abs.
-- Result: Returns a new Vec instance after performing the specified element-wise mathematical operation(s).

instance Num Vec where
    (+) :: Vec -> Vec -> Vec
    (Vec a) + (Vec b) = Vec (zipWith (+) a b)
    (-) :: Vec -> Vec -> Vec
    (Vec a) - (Vec b) = Vec (zipWith (-) a b)
    (*) :: Vec -> Vec -> Vec
    (Vec a) * (Vec b) = Vec (zipWith (*) a b)
    abs :: Vec -> Vec
    abs (Vec a) = Vec (map abs a)
    signum :: Vec -> Vec
    signum (Vec a) = Vec (map signum a)
    fromInteger :: Integer -> Vec
    fromInteger x = Vec [fromInteger x]

-- Function Purpose: Implement comparison operations (==, <, >, <=, >=) for Vec instances.
-- Function Parameters: Two Vec instances for comparison operations.
-- Result: Returns a Boolean value indicating the result of the comparison operation between the Vec instances.

instance Eq Vec where
    (==) :: Vec -> Vec -> Bool
    (Vec a) == (Vec b) = and (zipWith (==) a b)

instance Ord Vec where
    compare :: Vec -> Vec -> Ordering
    compare (Vec a) (Vec b) = compare a b

-- Function Purpose: Define a typeclass VecT with a magnitude function for calculating the magnitude of vectors.
-- Function Parameters: A VecT instance.
-- Result: Calculates and returns the magnitude of the vector represented by the given VecT instance.

class VecT a where
    magnitude :: VecT a => a -> Double

instance VecT Vec where
    magnitude :: VecT Vec => Vec -> Double
    magnitude (Vec values) = sqrt (sum (map (^2) values))

-- Function Purpose: Implement the Semigroup typeclass for Vec to enable the combination of vectors using the <> operator.
-- Function Parameters: Two Vec instances for combination using the <> operator.
-- Result: Returns a new Vec instance after adding the corresponding elements of the input Vec instances.

instance Semigroup Vec where
    (<>) :: Vec -> Vec -> Vec
    (Vec a) <> (Vec b) = Vec (zipWith (+) a b)


-- Function Purpose: Implement the Monoid typeclass for Vec to define the identity element (mempty) and the mappend operation.
-- Function Parameters: Two Vec instances for combination using mappend.
-- Result: Returns a new Vec instance after adding the corresponding elements of the input Vec instances.

instance Monoid Vec where
    mempty :: Vec
    mempty = Vec (repeat 0.0) -- Identity element: a vector of zeros
    mappend :: Vec -> Vec -> Vec
    (Vec a) `mappend` (Vec b) = Vec (zipWith (+) a b)
