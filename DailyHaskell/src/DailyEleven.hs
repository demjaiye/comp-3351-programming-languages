module DailyEleven where
import Data.Either (isLeft)

-- Purpose: allLefts extracts Left values from a list of Either types.
-- Parameters: A list of Either values (Either a b).
-- Results: A list containing all Left values (a) from the input list.
allLefts :: [Either a b] -> [a]
allLefts = foldr (\eitherVal acc -> case eitherVal of
        Left leftVal -> leftVal : acc
        Right _ -> acc) []


-- Purpose: produceStringOrSum produces a String or the sum of two integers based on input Either values.
-- Parameters: Two Either values (Either String Integer).
-- Results: Either a String or the sum of the integers, or Left "Invalid input" for other cases.
produceStringOrSum :: Either String Integer -> Either String Integer -> Either String Integer
produceStringOrSum (Left str1) _ = Left str1
produceStringOrSum _ (Left str2) = Left str2
produceStringOrSum (Right num1) (Right num2) = Right (num1 + num2)
produceStringOrSum _ _ = Left "Invalid input"


-- Purpose: sumListOfEither sums integers or extracts the first String from a list of Either values.
-- Parameters: A list of Either values (Either String Integer).
-- Results: Either the first String, the sum of integers, or Right 0 for an empty list.
sumListOfEither :: [Either String Integer] -> Either String Integer
sumListOfEither xs = case filter isLeft xs of
        [] -> Right (sum $ rights xs)
        (Left str):_ -> Left str
        where
            rights = map (\(Right x) -> x) . filter isRight
            isRight (Right _) = True
            isRight _ = False