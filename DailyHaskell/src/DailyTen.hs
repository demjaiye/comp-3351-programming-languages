module DailyTen where

-- Purpose: Check if the first functor law holds for a Functor instance.
-- Parameters: x - A value of a Functor type f a.
-- Result: Returns True if the first functor law holds, which means that applying 'fmap id' to x is equal to x.
firstFunctorLaw :: (Eq (f a), Functor f) => f a -> Bool
firstFunctorLaw x = fmap id x == x

-- Purpose: Check if the second functor law holds for a Functor instance.
-- Parameters: f - A function from type b to type c, g - A function from type a to type b, functor - A value of a Functor type f a.
-- Result: Returns True if the second functor law holds, which means that composing f and g and then applying fmap to the functor is 
-- equal to first applying fmap to g and then fmap to f on the functor.
secondFunctorLaw :: (Eq (f c), Functor f) => (b -> c) -> (a -> b) -> f a -> Bool
secondFunctorLaw f g functor = fmap (f . g) functor == (fmap f . fmap g) functor


-- Purpose: Define a Functor instance for MyEither, allowing mapping functions over the values contained in the data type.
-- Parameters: f - A function from type 'b' to type 'c', a - A value of type 'a' contained in MyLeft, f x - A function applied to the value of type 'b' contained in MyRight.
-- Result: A new MyEither instance with 'a' preserved in MyLeft and 'f x' applied to the value of type 'b' in MyRight.
-- data MyEither a b = MyLeft a | MyRight (Maybe b) deriving (Show, Eq)
-- instance Functor (MyEither a) where
--     fmap :: (b -> c) -> MyEither a b -> MyEither a c
--     fmap _ (MyLeft a) = MyLeft a
--     fmap f (MyRight Nothing) = MyRight Nothing
--     fmap f (MyRight (Just x)) = MyRight (Just (f x))




-- data MyEither a b = MyLeft a | MyRight (Maybe b) deriving (Show, Eq)

-- instance Functor (MyEither a) where
--     fmap :: (b -> c) -> MyEither a b -> MyEither a c
--     fmap f (MyLeft a) = MyLeft a
--     fmap f (MyRight Nothing) = MyRight Nothing
--     fmap f (MyRight (Just x)) = MyRight (Just (f x))