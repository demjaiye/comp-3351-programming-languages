module Practice where   

absValue :: Int -> Int
absValue 0 = 0
absValue x
    | x >0 = x
    | otherwise = (-x)


power :: Int -> Int -> Int
power _ 0 = 1
power x 1 = x
power x p = 
    let
        
        prevPower = power x (p-1)
    in
        x * prevPower

