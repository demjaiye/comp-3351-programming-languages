module DailyOne where
 -- Purpose: This quadratic function takes in 4 parameters of Int datatypes and then returns the quadratic value (Int datatype) of a + b*x + c*x^2
 -- Parameters: a, b, c and x 
 -- Result: Function produces an Int quadratic value
 quadratic :: Int -> Int -> Int -> Int -> Int
 quadratic a b c x = a + b * x + c * x^2
 first :: IO ()
 first = do
    putStrLn "The quadratic result is: "
    print (quadratic 5 3 2 1)

    print (quadratic 1 2 3 4)

    print (quadratic 1 0 1 0)

-- Purpose: This scaleVector function takes in a single number with a 2-tuple of Int datatypes and returns a 2-tuple (Int datatype) representing the vector scaled by the value
-- Parameters: a (b,c) 
-- Result: Function produces a 2-tuple vector
 scaleVector :: Int -> (Int, Int) -> (Int, Int)
 scaleVector a (b, c) = (a*b, a*c)
 second :: IO ()
 second = do
    putStrLn "The vector scaled by the value is: "
    print (scaleVector 5 (3, 4))

    print (scaleVector 3 (1, 1))

    print (scaleVector 1 (0, 1))


-- Purpose: This tripleDistance function takes in two 3-tuples (Int datatypes) representing 3-dimensional points and finds the cartesian distance between them
-- Parameters: (x1, y1, z1) (x2, y2, z2)
-- Result: Function produces the cartesian distance between two 3-tuples which has a double datatype
 tripleDistance :: (Int, Int, Int) -> (Int, Int, Int) -> Double
 tripleDistance (x1, y1, z1) (x2, y2, z2) = sqrt(fromIntegral((x2-x1)^2 + (y2-y1)^2 + (z2-z1)^2)) 
 third :: IO ()
 third = do
   putStrLn "The distance between two 3-dimensional points is: "

   print (tripleDistance (1 ,0 ,0) (0 ,0 ,1))

   print (tripleDistance (2 ,1 ,0) (0 ,0 ,0))

   print (tripleDistance (0 ,0 ,0) (4 ,9 ,16))





