module WeeklyHaskellOne where

-- Purpose: removeChar removes occurrences of a specified character from a string.
-- Parameters: charToRemove - the character to remove, (x:xs) - the input string.
-- Result: A new string with all instances of charToRemove removed.
removeChar :: Char -> String -> String
removeChar _ [] = []
removeChar charToRemove (x:xs)
    | x == charToRemove = removeChar charToRemove xs
    | otherwise = x : removeChar charToRemove xs



-- Purpose: Removes whitespace characters (space, tab, newline, carriage return) from a given string.
-- Parameters: Takes a single parameter, which is the input string from which to remove whitespace characters.
-- Result: Returns a new string with all space, tab, newline, and carriage return characters removed.
removeSpace :: String -> String
removeSpace = removeChar ' '

removeTab :: String -> String
removeTab = removeChar '\t'

removeNewline :: String -> String
removeNewline = removeChar '\n'

removeCarriageReturn :: String -> String
removeCarriageReturn = removeChar '\r'

removeWhitespace :: String -> String
removeWhitespace = removeSpace . removeTab . removeNewline . removeCarriageReturn



-- Purpose: Removes all punctuations (comma, period, parenthesis, square bracket, curly bracket) from the input string.
-- Parameters: Takes a single parameter of type 'String'.
-- Result: Returns a new string with all comma, period, parenthesis, square bracket, curly bracket removed.
removeComma :: String -> String
removeComma = removeChar ','

removePeriod :: String -> String
removePeriod = removeChar '.'

removeParentheses :: String -> String
removeParentheses = removeChar '(' . removeChar ')'

removeSquare :: String -> String
removeSquare = removeChar '[' . removeChar ']'

removeCurly :: String -> String
removeCurly = removeChar '{' . removeChar '}'

removePunctuation :: String -> String
removePunctuation = removeComma . removePeriod . removeParentheses . removeSquare . removeCurly



-- Purpose: Convert a string of characters to a list of their corresponding ASCII values.
-- Parameters: A string 'x:xs' to be converted where 'x' is the current character, and 'xs' is the rest of the string.
-- Result: Returns a list of integers, each representing the ASCII value of a character from the input string.
charToAscii :: Char -> Int
charToAscii = fromEnum
charsToAscii :: String -> [Int]
charsToAscii [] = [] 
charsToAscii (x:xs) = charToAscii x : charsToAscii xs 



-- Purpose: Converts a list of ASCII values to a string of characters.
-- Parameters: Takes a list of integers representing ASCII values.
-- Result: Returns a string where each integer in the list is converted to its corresponding character.
asciiToChar :: Int -> Char
asciiToChar = toEnum
asciiToChars :: [Int] -> String
asciiToChars [] = [] 
asciiToChars (x:xs) = asciiToChar x : asciiToChars xs 



-- Purpose: Shifts a list of integers by a specified amount within the ASCII range.
-- Parameters: 
--    shiftValue: The amount by which each integer in the list should be shifted.
--    (x:xs): The list of integers to be shifted.
-- Result: Returns a new list of integers where each value in the input list has been increased by the shift value (modulo 128), preserving ASCII range.
shiftInt :: Int -> Int -> Int
shiftInt shiftValue x = 
    let result = x + shiftValue
    in if result >= 0 && result <= 127
       then result
       else (result `mod` 128 + 128) `mod` 128
shiftInts :: Int -> [Int] -> [Int]
shiftInts _ [] = [] 
shiftInts shiftValue (x:xs) = shiftInt shiftValue x : shiftInts shiftValue xs 



-- Purpose: Shifts the characters in a given string by a specified ASCII value.
-- Parameters: 
--    shiftValue: The amount to shift each character in the string.
--    (x:xs): The input string represented as a list of characters.
-- Result: Returns a new string where each character has been shifted by the specified ASCII value.
shiftMessage :: Int -> String -> String
shiftMessage _ [] = [] 
shiftMessage shiftValue (x:xs) = asciiToChar (shiftInt shiftValue (charToAscii x)) : shiftMessage shiftValue xs



-- Purpose: Shifts the characters in a given string by a specified ASCII value using composition.
-- Parameters:
--    shiftValue: The amount to shift each character in the string.
--    charToAscii: Function to convert a character to its ASCII value.
--    shiftInt: Function to shift an ASCII value.
--    asciiToChar: Function to convert an ASCII value back to a character.
-- Result: Returns a new string where each character has been shifted by the specified ASCII value.
compositionalShift :: Int -> String -> String
compositionalShift shiftValue = map (asciiToChar . shiftInt shiftValue . charToAscii)



-- Purpose: Removes all characters from the input string that are present in the charsToRemove list.
-- Parameters:
--   - charsToRemove: The list of characters to be removed from the inputString.
--   - inputString: The string from which characters should be removed.
-- Result: Returns a new string with all characters from inputString except those present in charsToRemove.

-- The removeAllChars function has a linear time complexity of O(n), where n is the length of the input string.
-- Function composition with removeChar would have an asymptotic complexity of O(k*n) when composed k times. 
-- removeAllChars function generally performs well with a linear time complexity, making it an efficient solution for character removal. 
-- In conclusion, The removeAllChars function is asymptotically faster than composing removeChar functions multiple times, especially when there are many characters to remove.
removeAllChars :: [Char] -> [Char] -> [Char]
removeAllChars charsToRemove inputString = filter (\c -> not (c `elem` charsToRemove)) inputString
