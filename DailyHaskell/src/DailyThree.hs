module DailyThree where


-- Purpose: 'removeAllExcept' is a function that filters elements in a list (thisList) and retains only those equal to the specified element (compares).
-- Parameters: 
--   compares: The element to compare against in the list for filtering.
--   thisList: The input list from which elements will be filtered.
-- Result: The result of removeAllExcept is a new list containing elements that match the 'compares' element.
removeAllExcept :: Eq a => a -> [a] -> [a]
removeAllExcept compares thisList = remove compares thisList []
  where
    remove :: Eq a => a -> [a] -> [a] -> [a]
    remove _ [] acc = reverse acc
    remove comp (x:xs) acc
      | comp == x = remove comp xs (x : acc)
      | otherwise = remove comp xs acc


-- Purpose: 'countOccurrence' is a function that counts the number of occurrences of a specific element (compares) in a list (xs).
-- Parameters: 
--   compares: The element whose occurrences are being counted.
--   xs: The input list in which occurrences of 'compares' are counted.
-- Result: The result of countOccurrence is an integer representing the count of occurrences of 'compares' in the list 'xs'.
countOccurrences :: Eq a => a -> [a] -> Int
countOccurrences _ [] = 0  
countOccurrences compares (x:xs)
  | compares == x = 1 + countOccurrences compares xs  
  | otherwise = countOccurrences compares xs         


-- Purpose: 'substitute' is a function that performs substitutions in a list by replacing occurrences of one element (arg1) with another element (arg2).
-- Parameters: 
--   arg1: The element to be replaced in the list.
--   arg2: The element to replace 'arg1' with.
--   xs: The input list in which substitutions will be made.
-- Result: The result of 'substitute' is a new list where every occurrence of 'arg1' in 'xs' is replaced with 'arg2', while all other elements remain unchanged.
substitute :: Eq a => a -> a -> [a] -> [a]
substitute _ _ [] = [] 
substitute arg1 arg2 (x:xs)
  | arg1 == x = arg2 : substitute arg1 arg2 xs 
  | otherwise = x : substitute arg1 arg2 xs   


-- Purpose: 'setListDiff' is a function that computes the difference between two lists ('list1' and 'list2') by producing elements present in 'list2' but not present in 'list1'.
-- Parameters: 
--   list1: The first list used for comparison.
--   list2: The second list from which elements found in 'list1' will be removed.
-- Result: The result of 'setListDiff' is a new list containing elements from 'list2' that are not present in 'list1'.
setListDiff :: Eq a => [a] -> [a] -> [a]
setListDiff _ [] = [] 
setListDiff list1 list2 = filterNotInList1 list2 list1 []
  where
    filterNotInList1 :: Eq a => [a] -> [a] -> [a] -> [a]
    filterNotInList1 [] _ result = result 
    filterNotInList1 (x:xs) list1 result
      | x `elem` list1 = filterNotInList1 xs list1 result 
      | otherwise = filterNotInList1 xs list1 (result ++ [x])  

