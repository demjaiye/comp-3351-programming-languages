module TriTree where

data TriTree a = Empty | Leaf a | Node a a (TriTree a) (TriTree a) (TriTree a)
    deriving (Show, Eq)

-- Purpose: To search for a specific value in a TriTree.
-- Parameter: The target value to be searched and a TriTree to search in.
-- Result: Returns True if the target value is found in the TriTree, otherwise returns False.
search :: Ord a => a -> TriTree a -> Bool
search _ Empty = False
search target (Leaf val) = target == val
search target (Node val1 val2 lc mc rc)
    | target == val1 || target == val2 = True
    | target < val1 = search target lc
    | target > val2 = search target rc
    | otherwise = search target mc


-- Purpose: To insert a value into a TriTree while maintaining the order properties.
-- Parameter: The value to be inserted and an existing TriTree.
-- Result: Returns a new TriTree with the inserted value.
insert :: Ord a => a -> TriTree a -> TriTree a
insert value Empty = Leaf value
insert value (Leaf val)
    | value <= val = Node value val (Leaf value) (Leaf val) Empty
    | otherwise = Node val value (Leaf val) (Leaf value) Empty
insert value (Node val1 val2 lc mc rc)
    | value < val1 = Node value val2 (insert value lc) mc rc
    | value > val2 = Node val1 val2 lc mc (insert value rc)
    | value <= val2 = Node val1 value lc (insert value mc) rc
    | otherwise = Node val1 val2 lc mc (insert value rc)


-- Purpose: To insert a list of values into a TriTree while maintaining the order properties.
-- Parameter: A list of values and an existing TriTree.
-- Result: Returns a new TriTree with the inserted values.
insertList :: Ord a => [a] -> TriTree a -> TriTree a
insertList values tree = foldr insert tree values


-- Purpose: To determine if two TriTrees are identical in structure and values.
-- Parameter: Two TriTrees to be compared.
-- Result: Returns True if the TriTrees are identical, otherwise returns False.
identical :: Eq a => TriTree a -> TriTree a -> Bool
identical Empty Empty = True
identical (Leaf val1) (Leaf val2) = val1 == val2
identical (Node x1 y1 lc1 mc1 rc1) (Node x2 y2 lc2 mc2 rc2) =
    x1 == x2 && y1 == y2 &&
    identical lc1 lc2 && identical mc1 mc2 && identical rc1 rc2
identical _ _ = False


-- treeMap function applies a given function to each value in a TriTree.
-- Parameters: a function (a -> b) and a TriTree of type a.
-- Result: Returns a new TriTree where the given function is applied to every value.
treeMap :: (a -> b) -> TriTree a -> TriTree b
treeMap _ Empty = Empty
treeMap f (Leaf val) = Leaf (f val)
treeMap f (Node val1 val2 lc mc rc) =
    Node (f val1) (f val2) (treeMap f lc) (treeMap f mc) (treeMap f rc)


-- treeFoldPreOrder function combines values in a TriTree using a given function in pre-order traversal.
-- Parameters: a combining function (a -> b -> a), an initial accumulator value, and a TriTree of type b.
-- Result: Returns the accumulated result after applying the function in pre-order traversal.
treeFoldPreOrder :: (a -> b -> a) -> a -> TriTree b -> a
treeFoldPreOrder _ acc Empty = acc
treeFoldPreOrder f acc (Leaf val) = f acc val
treeFoldPreOrder f acc (Node val1 val2 lc mc rc) =
    let acc1 = f acc val1
        acc2 = f acc1 val2
        accLc = treeFoldPreOrder f acc2 lc
        accMc = treeFoldPreOrder f accLc mc
    in treeFoldPreOrder f accMc rc


-- treeFoldInOrder function combines values in a TriTree using a given function in in-order traversal.
-- Parameters: a combining function (a -> b -> a), an initial accumulator value, and a TriTree of type b.
-- Result: Returns the accumulated result after applying the function in in-order traversal.
treeFoldInOrder :: (a -> b -> a) -> a -> TriTree b -> a
treeFoldInOrder _ acc Empty = acc
treeFoldInOrder f acc (Leaf val) = f acc val
treeFoldInOrder f acc (Node val1 val2 lc mc rc) =
    let accLc = treeFoldInOrder f acc lc
        accVal1 = f accLc val1
        accMc = treeFoldInOrder f accVal1 mc
        accVal2 = f accMc val2
    in treeFoldInOrder f accVal2 rc


-- treeFoldPostOrder function combines values in a TriTree using a given function in post-order traversal.
-- Parameters: a combining function (a -> b -> a), an initial accumulator value, and a TriTree of type b.
-- Result: Returns the accumulated result after applying the function in post-order traversal.
treeFoldPostOrder :: (a -> b -> a) -> a -> TriTree b -> a
treeFoldPostOrder _ acc Empty = acc
treeFoldPostOrder f acc (Leaf val) = f acc val
treeFoldPostOrder f acc (Node val1 val2 lc mc rc) =
    let accLc = treeFoldPostOrder f acc lc
        accRc = treeFoldPostOrder f accLc rc
        accMc = treeFoldPostOrder f accRc mc
        accVal2 = f accMc val2
    in f accVal2 val1


-- Purpose: Deletes a specified value from the TriTree.
-- Parameter: The value to be deleted and the TriTree from which to delete.
-- Result: Returns a new TriTree with the specified value removed, maintaining the ordering properties.
delete :: Ord a => a -> TriTree a -> TriTree a
delete _ Empty = Empty
delete val (Leaf x)
    | val == x = Empty
    | otherwise = Leaf x
delete val (Node val1 val2 lc mc rc)
    | val < val1 = Node val1 val2 (delete val lc) mc rc
    | val > val2 = Node val1 val2 lc mc (delete val rc)
    | val == val1 && val == val2 = Node val1 val2 (delete val lc) mc rc 
    | val == val1 = Node val2 val2 (delete val lc) mc rc 
    | val == val2 = Node val1 val1 lc mc rc 
    | otherwise = Node val1 val2 (delete val lc) mc rc

-- Purpose: Builds a balanced TriTree from a list of values.
-- Parameter: The list of values to build the TriTree from.
-- Result: Returns a TriTree with the values from the input list, organized to maintain the ordering properties.
splitList :: [a] -> ([a], [a])
splitList xs = splitAt (length xs `div` 2) xs

buildTree :: [a] -> TriTree a
buildTree [] = Empty
buildTree [x] = Leaf x
buildTree xs =
    let (left, (val1 : middleAndVal2)) = splitList xs
        (middle, [val2]) = splitList middleAndVal2
        lc = buildTree left
        mc = buildTree middle
        rc = buildTree (tail middleAndVal2)
    in Node val1 val2 lc mc rc


