module DailyNine where

-- Purpose: The 'findSmallest' function takes a list of comparable elements as input and finds the smallest element in the list.
-- Parameters: Function takes a list [a]
-- Result: It returns 'Nothing' if the input list is empty, returns element if list has one element, it compares elements recursively and returns the smallest element if list has more than one element
findSmallest :: (Ord a) => [a] -> Maybe a
findSmallest [] = Nothing
findSmallest [x] = Just x
findSmallest (x:xs) =
    case minNum of
        Just y -> if x < y then Just x else minNum
        Nothing -> Just x
    where minNum = findSmallest xs

-- Purpose: The 'allTrue' function takes a list of booleans as input and checks if all elements in the list are 'True'. If all elements are 'True', it returns 'Just True'; otherwise, it returns 'Just False'. If the input list is empty, it returns 'Nothing'.
-- Parameters: A list of booleans, denoted as [Bool], where each element represents a truth value.
-- Result: It returns 'Just True' if all elements in the input list are 'True'. If any element is 'False', it returns 'Just False'. If the input list is empty, it returns 'Nothing', indicating no specific truth value can be determined.
allTrue :: [Bool] -> Maybe Bool
allTrue [] = Nothing
allTrue [True] = Just True
allTrue [False] = Just False
allTrue (x:xs)
    | x && allTrue xs == Just True = Just True
    | otherwise = Just False


-- Purpose: The function 'countAllVotes' takes a list of Maybe Bool values and counts the occurrences of Just True, Just False, and Nothing in the input list.
-- Parameters: A list of Maybe Bool values representing votes.
-- Result: A tuple containing three integer: count of Just True, Just False and Nothing votes
countAllVotes :: [Maybe Bool] -> (Integer, Integer, Integer)
countAllVotes [] = (0,0,0)
countAllVotes (x:xs)
    | x == Just True = (counterN, counterT+1, counterF)
    | x == Just False = (counterN, counterT, counterF+1)
    | otherwise = (counterN+1, counterT, counterF)
    where
        (counterN, counterT, counterF) = countAllVotes xs
        
